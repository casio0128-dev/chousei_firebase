import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyC2ZCCEHRPWFZFDegmE_1ukVkYi6uDW4LM",
  authDomain: "casio-chousei.firebaseapp.com",
  databaseURL: "https://casio-chousei.firebaseio.com",
  projectId: "casio-chousei",
  storageBucket: "",
  messagingSenderId: "837923081708",
  appId: "1:837923081708:web:32211a2cd817d80c"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
